// Soal No. 1 (Array to Object)
const arrayToObject = (array) => {
    if (array) {
        const now = new Date(),
              thisYear = now.getFullYear();
        let no = 1,
            data = '';
        array.forEach(e => {
            let getNo = no++,
                dataObj = {
                    firstName: e[0],
                    lastName: e[1],
                    gender: e[2],
                    age: e[3] ? thisYear - e[3] : "Invalid Birth Year"
                };
            data += getNo+'. '+e[0]+' '+e[1]+':'+JSON.stringify(dataObj, null, 4)+'\n';
        });
        console.log(data);
    } else {
        console.log('');
    }
}

let people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people); 
let people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2); 
arrayToObject([]);

// Soal No. 2 (Shopping Time)
const shoppingTime = (memberId, money) => {
    let data = '';
    if (typeof memberId === 'undefined' || typeof money === 'undefined') {
        data = 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else {
        if (money < 50000 ) {
            data += "Mohon maaf, uang tidak cukup";
        } else {
            if (memberId.length) {
                if (money < 175000) {
                    const dataObj = {
                        memberId: memberId,
                        money: money,
                        listPurchased: [
                            'Casing Handphone',
                        ],
                        changeMoney: money-50000
                    };
                    data += JSON.stringify(dataObj, null, 2);
                } else if (money < 250000) {
                    const dataObj = {
                        memberId: memberId,
                        money: money,
                        listPurchased: [
                            'Casing Handphone',
                        ],
                        changeMoney: money-50000-175000
                    };
                    data += JSON.stringify(dataObj, null, 2);
                } else if (money < 500000) {
                    const dataObj = {
                        memberId: memberId,
                        money: money,
                        listPurchased: [
                            'Casing Handphone',
                        ],
                        changeMoney: money-50000-175000-250000
                    };
                    data += JSON.stringify(dataObj, null, 2);
                } else {
                    const dataObj = {
                        memberId: memberId,
                        money: money,
                        listPurchased: [ 
                            'Sepatu Stacattu',
                            'Baju Zoro',
                            'Baju H&N',
                            'Sweater Uniklooh',
                            'Casing Handphone' 
                        ],
                        changeMoney: money-1500000-500000-250000-175000-50000 
                    };
                    data += JSON.stringify(dataObj, null, 2);
                }
            } else {
                data += "Mohon maaf, toko X hanya berlaku untuk member saja";
            }
        }
    }
    
    return data;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

// Soal No. 3 (Naik Angkot)
const naikAngkot = (listPenumpang) => {
    const rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    if (listPenumpang.length) {
        let data = [];
        listPenumpang.forEach(e => {
            let bayar = 0,
                status = false;
            rute.forEach(el => {
                if (e[1] === el) {
                    bayar += 2000;
                    status = true;
                } else {
                    if (status === true) {
                        if (e[2] !== el) {
                            bayar += 2000;
                        } else {
                            status = false;
                        }
                    }
                }
            });
            dataObj = {
                penumpang: e[0],
                naikDari: e[1],
                tujuan: e[2],
                bayar: bayar
            }
            data.push(dataObj);
        });
        return JSON.stringify(data, null, 4);
    } else {
        return listPenumpang;
    }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]