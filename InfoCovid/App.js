import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Login from './src/pages/Login'
import Register from './src/pages/Register';
import Dashboard from './src/pages/Dashboard';
import Splash from './src/pages/Splash';
import About from './src/pages/About'
import firebase from '@react-native-firebase/app';

var firebaseConfig = {
  apiKey: "AIzaSyDBNA2zYpKUIh_gmu1ETVKI70df3F1rJ80",
  authDomain: "latihan-auth-4cf4e.firebaseapp.com",
  projectId: "latihan-auth-4cf4e",
  storageBucket: "latihan-auth-4cf4e.appspot.com",
  messagingSenderId: "389812790036",
  appId: "1:389812790036:web:3584a52f752ce5f3d61801",
  measurementId: "G-PTNHPR5JHG"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
          <Stack.Navigator initialRouteName="Splash">
            <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}} />
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Home" component={Home} options={{headerShown: false}} />
          </Stack.Navigator>
      </NavigationContainer>
  )
}

const Home = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Dashboard" component={Dashboard} />
      <Tab.Screen name="About" component={About} />
    </Tab.Navigator>
  );
}

export default App;