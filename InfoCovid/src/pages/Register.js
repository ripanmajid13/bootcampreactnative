import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput, Alert } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import auth from '@react-native-firebase/auth';

const Register = ({navigation}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const onPressRegister = () => {
        if (email == '' || password == '') {
            Alert.alert('Error', 'Form tidak boleh kosong')
        } else {
            auth().createUserWithEmailAndPassword(email, password)
            .then((res) => {
                alert('Berhasil')
                setEmail('')
                setPassword('')
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            })
        }
    }

    const Login = () => {
        navigation.navigate('Login')
    }

    return (
        <View style={{flex: 1}}>
            <ScrollView
                    contentContainerStyle={styles.contenScrollView}
                    showsVerticalScrollIndicator={false}
                    contentInsetAdjustmentBehavior="automatic">
                <View style={styles.wrapper}>
                    <TextInput 
                        style={styles.input} 
                        placeholderTextColor={'#A5A5A5'} 
                        placeholder={'Email'}
                        value={email}
                        onChangeText={(text) => setEmail(text)}
                    />
                    <TextInput 
                        style={styles.input} 
                        placeholderTextColor={'#A5A5A5'} 
                        placeholder={'Password'} 
                        value={password}
                        secureTextEntry
                        onChangeText={(text) => setPassword(text)}
                    />
                    <View style={{  alignItems: 'center'}}>
                        <TouchableOpacity onPress={onPressRegister} style={{ width: 200, backgroundColor: '#2BBBAD', borderRadius: 25, paddingVertical: 13, marginTop: 50}}>
                            <Text style={{textAlign: 'center', color: 'white', fontWeight: 'bold', fontSize: 17}}>Register</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={Login} style={{ width: 200, backgroundColor: '#2BBBAD', borderRadius: 25, paddingVertical: 13, marginTop: 5}}>
                            <Text style={{textAlign: 'center', color: 'white', fontWeight: 'bold', fontSize: 17}}>Login</Text>
                        </TouchableOpacity>
                        
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    wrapper: {
        flex: 1, 
        justifyContent: 'center', 
        paddingHorizontal: 20, 
        marginVertical: 30
    },
    desc:{
        color: '#2BBBAD', 
        marginVertical: 10, 
        fontSize: 15
    },
    input:{
        borderBottomWidth: 1, 
        borderBottomColor: '#A5A5A5', 
        fontSize: 14, 
        padding: 0, 
        marginTop: 33
    },
    contenScrollView: {
        flexGrow : 1, 
        justifyContent : 'center'
    }
})

