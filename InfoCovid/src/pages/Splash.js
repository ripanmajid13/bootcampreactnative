import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

const Splash = ({navigation}) => {
    setTimeout(() => {
        navigation.replace("Login")
    }, 1000);
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Image
                style={{ width: 200, height: 200 }}
                source={{
                    uri: 'http://swarapendidikan.um.ac.id/wp-content/uploads/2020/11/virus.png',
                }}
            />
            <Text style={{fontSize: 30, color: '#2BBBAD', fontWeight: 'bold'}}>Info Covid</Text>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({})
