import React, { useEffect, useState } from 'react'
import { FlatList, SafeAreaView, StyleSheet, Text, View } from 'react-native'
import auth from '@react-native-firebase/auth';
import axios from 'axios'

const Home = () => {
    const [user, setUser] = useState({})
    const [dataIndonesia, setDataIndonesia] = useState({})
    const [dataProvinsi, setDataProvinsi] = useState({})
    
    useEffect(() => {
        const userInfo = auth().currentUser
        setUser(userInfo)

        getDataIndonesia();
        getDataProvinsi();
    }, [])

    const getDataIndonesia = () => {
        axios.get('https://api.kawalcorona.com/indonesia')
        .then((res) => {
            setDataIndonesia(res.data)
        })
        .catch(function (error) {
            console.log(error);
        })
    }

    const getDataProvinsi = () => {
        axios.get('https://api.kawalcorona.com/indonesia/provinsi')
        .then((res) => {
            setDataProvinsi(res.data)
        })
        .catch(function (error) {
            console.log(error);
        })
    }

    const renderGlobal = ({ item }) => (
        <View 
            style={{
                marginTop: 10, 
                backgroundColor: '#3EDEEE', 
                marginHorizontal: 10, 
                padding: 10,
                borderRadius: 5
            }}>
            <Text style={{fontSize: 20, color: '#FFFFFF', marginBottom: 10}}>Data Global</Text>
            <Text>Positif : {item.positif}</Text>
            <Text>Sembuh : {item.sembuh}</Text>
            <Text>Meninggal : {item.meninggal}</Text>
        </View>
    );

    const renderProvinsi = ({ item }) => (
        <View 
            style={{
                marginTop: 10, 
                backgroundColor: '#A5A5A5', 
                marginHorizontal: 10, 
                padding: 10,
                borderRadius: 5
            }}>
            <Text style={{fontSize: 15, color: '#FFFFFF', marginBottom: 10}}>Provinsi {item.attributes.Provinsi}</Text>
            <Text>Positif : {item.attributes.Kasus_Posi}</Text>
            <Text>Sembuh : {item.attributes.Kasus_Semb}</Text>
            <Text>Meninggal : {item.attributes.Kasus_Meni}</Text>
        </View>
    );

    return (
        <View>
            <View style={{alignItems: 'center', paddingVertical: 15, borderBottomWidth: 1, borderBottomColor: '#A5A5A5'}}>
                <Text style={{fontSize: 20}}>Data Informasi Corona Indonesia</Text>
            </View>

            <SafeAreaView style={{marginBottom: 10}}>
                <FlatList
                    data={dataIndonesia}
                    renderItem={renderGlobal}
                    keyExtractor={item => item.name}
                />
            </SafeAreaView>

            <SafeAreaView>
                <FlatList
                    data={dataProvinsi}
                    renderItem={renderProvinsi}
                    keyExtractor={item => item.Kode_Provi}
                />
            </SafeAreaView>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({})
