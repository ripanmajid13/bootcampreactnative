import React, {useState} from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import auth from '@react-native-firebase/auth';

const Login = ({navigation}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const onPressLogin = () => {
        if (email == '' || password == '') {
            alert('Form tidak boleh kosong')
        } else {
            auth().signInWithEmailAndPassword(email, password)
            .then((res) => {
                navigation.navigate('Home')
            })
            .catch((err) => {
                console.log(err);
            })
        }
    }


    const Register = () => {
        navigation.navigate('Register')
    }

    return (
        <View style={{flex: 1}}>
            <ScrollView
                    contentContainerStyle={styles.contenScrollView}
                    showsVerticalScrollIndicator={false}
                    contentInsetAdjustmentBehavior="automatic">
                <View style={styles.wrapper}>
                <TextInput 
                        style={styles.input} 
                        placeholderTextColor={'#A5A5A5'} 
                        placeholder={'Email'}
                        value={email}
                        onChangeText={(text) => setEmail(text)}
                    />
                    <TextInput 
                        style={styles.input} 
                        placeholderTextColor={'#A5A5A5'} 
                        placeholder={'Password'} 
                        value={password}
                        secureTextEntry
                        onChangeText={(text) => setPassword(text)}
                    />
                    <View style={{  alignItems: 'center'}}>
                        <TouchableOpacity onPress={onPressLogin} style={{ width: 200, backgroundColor: '#2BBBAD', borderRadius: 25, paddingVertical: 13, marginTop: 50}}>
                            <Text style={{textAlign: 'center', color: 'white', fontWeight: 'bold', fontSize: 17}}>Login</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={Register} style={{ width: 200, backgroundColor: '#2BBBAD', borderRadius: 25, paddingVertical: 13, marginTop: 5}}>
                            <Text style={{textAlign: 'center', color: 'white', fontWeight: 'bold', fontSize: 17}}>Register</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    wrapper: {
        flex: 1, 
        justifyContent: 'center', 
        paddingHorizontal: 20, 
        marginVertical: 30
    },
    desc:{
        color: '#2BBBAD', 
        marginVertical: 10, 
        fontSize: 15
    },
    input:{
        borderBottomWidth: 1, 
        borderBottomColor: '#A5A5A5', 
        fontSize: 14, 
        padding: 0, 
        marginTop: 33
    },
    contenScrollView: {
        flexGrow : 1, 
        justifyContent : 'center'
    }
})
