// 1. Animal Class 
class Animal { 
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
}

// Release 0
var sheep = new Animal("shaun");
 
// console.log(sheep.name);
// console.log(sheep.legs);
// console.log(sheep.cold_blooded);

// Release 1
class Ape extends Animal {
    yell = () => {
        console.log("Auooo");
    };
}

class Frog extends Animal {
    jump = () => {
        console.log("hop hop");
    };
}

// var sungokong = new Ape("kera sakti");
// sungokong.yell();

// var kodok = new Frog("buduk");
// kodok.jump();

// 2. Function to Class
class Clock {
    constructor({template}) {
        this.template = template;
        this.timer;
    }
  
    render = () => {
      let date = new Date();
  
      let hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      let mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      let secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop = () => {
      clearInterval(this.timer);
    };
  
    start = () => {
      this.render();
      this.timer = setInterval(this.render, 1000);
    };
  
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 