import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button } from "react-native";

export default function Login({ navigation }) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const submit = () => {
    setIsLoading(true);

    setTimeout(() => {
      if (password !== '12345678') {
        setIsError(true);
        setIsLoading(false);
        setUsername("");
        setPassword("");
        setTimeout(() => {
          setIsError(false);
        }, 1000);
      } else {
        navigation.navigate("Home", {
          username: 'Ripan'
        });
      } 
    }, 1000);
  };

  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 20, fontWeight: "bold" }}>== Quiz 3 ==</Text>
      <Image
        style={{ height: 150, width: 150 }}
        source={require("./assets/logo.jpg")}
      />
      { isError ? <Text style={{ marginBottom: 3, color: 'red' }}>Login Gagal</Text> : null }
      { isLoading ? <Text style={{ marginBottom: 3}}>Loading</Text> : null }
      <View>
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Username"
          value={username}
          onChangeText={(value)=>setUsername(value)}
        />
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Password"
          value={password}
          onChangeText={(value)=>setPassword(value)}
        />
        <Button onPress={(submit)} title="Login" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
});
