import React, { useEffect } from 'react'
import { useState } from 'react'
import { FlatList, StyleSheet, Text, View, Image, Button, ScrollView } from 'react-native'
import { Data }from './data'

export default function Home({route, navigation}) {
    const { username } = route.params;
    const [totalPrice, setTotalPrice] = useState(0);

    const currencyFormat=(num)=> {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      };

    const updateHarga =(price)=>{
        const temp = Number(price) + totalPrice;
        setTotalPrice(temp);
    }
    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>

            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
                <ScrollView
                    contentContainerStyle={{flexGrow : 1, justifyContent : 'center'}}
                    showsVerticalScrollIndicator={false}
                    contentInsetAdjustmentBehavior="automatic">
                    <FlatList
                        numColumns={2}
                        data={Data}
                        keyExtractor={(item) => item.id}
                        renderItem={({item}) => {
                            return (
                                <View style={styles.content}>
                                    <Text>{item.title}</Text>
                                    <Image
                                        style={{ height: 120, width: 120 }}
                                        source={item.image}
                                    />
                                    <Text>{item.harga}</Text>
                                    <Text>{item.type}</Text>
                                    <Button title="BELI" onPress={() => updateHarga(item.harga)} />
                                </View>
                            )
                        }}
                    />
                </ScrollView>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
        
})
