import React from 'react'
// import Telegram from './Tugas/Tugas12/Telegram';
// import { setCustomTextInput, setCustomText } from 'react-native-global-props';
// import RestApi from './Tugas/Tugas14/RestApi';
// import AboutScreen from './Tugas/Tugas13/AboutScreen';
// import LoginScreen from './Tugas/Tugas13/LoginScreen';
// import Tugas15 from './Tugas/Tugas15';
// import Quiz3 from './Quiz3';
import Auth from './Tugas/LatihanAuthentikasi';

import firebase from '@react-native-firebase/app';

var firebaseConfig = {
  apiKey: "AIzaSyDBNA2zYpKUIh_gmu1ETVKI70df3F1rJ80",
  authDomain: "latihan-auth-4cf4e.firebaseapp.com",
  projectId: "latihan-auth-4cf4e",
  storageBucket: "latihan-auth-4cf4e.appspot.com",
  messagingSenderId: "389812790036",
  appId: "1:389812790036:web:3584a52f752ce5f3d61801",
  measurementId: "G-PTNHPR5JHG"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const App = () => {
  // setCustomTextInput(customTextInputProps);
  // setCustomText(customTextProps);

  return (
    // <Telegram />
    // <LoginScreen />
    // <AboutScreen />
    // <RestApi />
    // <Tugas15 />
    // <Quiz3 />
    <Auth />
  )
}

// const customTextInputProps = {
//   style: {
//     fontFamily: 'Sintony-Regular',
//   }
// };

// const customTextProps = {
//   style: {
//     fontFamily: 'Sintony-Regular',
//   }
// };

export default App;