import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import auth from '@react-native-firebase/auth';

const Home = () => {
    const [user, setUser] = useState({})
    
    useEffect(() => {
        const userInfo = auth().currentUser
        setUser(userInfo)
    }, [])
    return (
        <View>
            <Text>Home</Text>
            <Text>Selamat data, {user.email}</Text>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({})
