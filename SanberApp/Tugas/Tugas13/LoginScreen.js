import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'
import TopBar from './components/TopBar';
import IconBack from './asset/arrow-left.svg';
import ImageLogin from './asset/logo-login.svg';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import auth from '@react-native-firebase/auth';

const LoginScreen = () => {
    return (
        <View style={{flex: 1}}>
            <TopBar content={<TopBarContent />} />
            <ScrollView
                    contentContainerStyle={styles.contenScrollView}
                    showsVerticalScrollIndicator={false}
                    contentInsetAdjustmentBehavior="automatic">
                <View style={styles.wrapper}>
                    <ImageLogin width={185} height={148} />
                    <Text style={styles.desc}>{'Mohon mengisi data, \nuntuk menggunakan akun anda.'}</Text>
                    <TextInput style={styles.input} placeholderTextColor={'#A5A5A5'} placeholder={'Email or Username'} />
                    <TextInput style={styles.input} placeholderTextColor={'#A5A5A5'} placeholder={'Password'} />
                    <View style={{alignItems: 'center'}}>
                        <TouchableOpacity style={{ width: 200, backgroundColor: '#2BBBAD', borderRadius: 25, paddingVertical: 13, marginTop: 50}}>
                            <Text style={{textAlign: 'center', color: 'white', fontWeight: 'bold', fontSize: 17}}>Login</Text>
                        </TouchableOpacity>
                        <Text style={{fontWeight: 'bold', color: '#2BBBAD', fontSize: 15, marginTop: 25}}>Lupa Password</Text>
                        <View style={{flexDirection: 'row',  marginTop: 20}}>
                            <Text style={{fontWeight: 'bold', color: '#A5A5A5', fontSize: 15}}>Belum memiliki akun ?, </Text>
                            <Text style={{fontWeight: 'bold', color: '#2BBBAD', fontSize: 15}}>Register.</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

const TopBarContent = () => {
    return (
        <View style={styles.topBarWrapper}>
            <View style={{flex: 1}}>
                <IconBack width={25} height={25} />
            </View>
            <View  style={{flex: 6}}>
                <Text style={styles.topBarTitle}>Login</Text>
            </View>
        </View>
    );
}

export default LoginScreen;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1, 
        justifyContent: 'center', 
        paddingHorizontal: 20, 
        marginVertical: 30
    },
    desc:{
        color: '#2BBBAD', 
        marginVertical: 10, 
        fontSize: 15
    },
    input:{
        borderBottomWidth: 1, 
        borderBottomColor: '#A5A5A5', 
        fontSize: 14, 
        padding: 0, 
        marginTop: 33
    },
    contenScrollView: {
        flexGrow : 1, 
        justifyContent : 'center'
    },
    topBarWrapper: {
        flexDirection: 'row', 
        flex: 1, 
        alignItems: 'center', 
        paddingHorizontal: 20
        // borderBottomColor: '#E5E5E5', 
        // borderBottomWidth: 1
    },
    topBarTitle: {
        fontSize: 23, 
        color: '#2BBBAD', 
        fontWeight: 'bold'
    }
})
