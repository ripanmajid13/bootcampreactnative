import React from 'react'
import { StyleSheet, View } from 'react-native'

const TopBar = ({content}) => {
    return (
        <View style={styles.wrapper}>
            {content}
        </View>
    )
}

export default TopBar

const styles = StyleSheet.create({
    wrapper: {
        height: 55,
        // backgroundColor: 'lime',
        // paddingHorizontal: 20,
    }
})
