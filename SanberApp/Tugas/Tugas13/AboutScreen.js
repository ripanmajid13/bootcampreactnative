import React from 'react'
import TopBar from './components/TopBar';
import IconDrawwer from './asset/align-justify.svg';
import IconGitlab from './asset/gitlab.svg';
import IconGithub from './asset/github.svg';
import IconLinkedin from './asset/linkedin.svg';
import IconFacebook from './asset/facebook.svg';
import IconInstagram from './asset/instagram.svg';
import IconHome from './asset/home.svg';
import IconNewspaper from './asset/newspaper.svg';
import IconHistory from './asset/history.svg';
import IconShopping from './asset/shopping-bag.svg';
import IconUser from './asset/user-circle.svg';
import ImageProfile from './asset/profile-image.svg';
import { StyleSheet, Text, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';

const AboutScreen = () => {
    return (
        <View style={{flex: 1}}>
            <TopBar content={<TopBarContent />} />
            <ScrollView
                contentContainerStyle={styles.contenScrollView}
                showsVerticalScrollIndicator={false}
                contentInsetAdjustmentBehavior="automatic">
                <View style={styles.wrapper}>
                    <View style={{ alignItems: 'center', marginBottom: 15 }}>
                        <ImageProfile style={{ marginBottom: 20 }} width={173} height={154} />
                        <Text style={{ color: '#2BBBAD', fontSize: 30 }}>Ripan Nur Alimajid</Text>
                        <Text style={{ color: '#2BBBAD', fontSize: 15 }}>React Native Developer</Text>
                    </View>

                    <View>
                        <Text style={{ marginBottom: 15, marginTop: 20, color: '#2BBBAD', fontSize: 15 }}>Portofolio</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <IconGitlab style={{ marginBottom: 10, marginRight: 20 }} width={20} height={20} />
                            <Text>@alimajid07</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconGithub style={{ marginBottom: 10, marginRight: 20 }} width={20} height={20} />
                            <Text>@alimajid07</Text>
                        </View>
                        

                        <Text style={{ marginBottom: 15, marginTop: 20, color: '#2BBBAD', fontSize: 15 }}>Social</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <IconLinkedin style={{ marginBottom: 10, marginRight: 20 }} width={20} height={20} />
                            <Text>@alimajid07</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconFacebook style={{ marginBottom: 10, marginRight: 20 }} width={20} height={20} />
                            <Text>@alimajid07</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconInstagram style={{ marginBottom: 10, marginRight: 20 }} width={20} height={20} />
                            <Text>@alimajid07</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <TopBar content={<Navigation />} />
        </View>
    )
}

const TopBarContent = () => {
    return (
        <View style={styles.topBarWrapper}>
            <View style={{flex: 6}}>
                <Text style={styles.topBarTitle}>Profile</Text>
            </View>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
                <IconDrawwer width={25} height={25} />
            </View>
        </View>
    );
}

const Navigation = () => {
    return (
        <View style={{ paddingHorizontal: 20, backgroundColor: '#F0F3F8', flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <IconHome width={25} height={25} />
            <IconNewspaper width={25} height={25} />
            <IconHistory width={25} height={25} />
            <IconShopping width={25} height={25} />
            <IconUser width={25} height={25} />
        </View>
    );
}

export default AboutScreen

const styles = StyleSheet.create({
    wrapper: {
        flex: 1, 
        justifyContent: 'center', 
        paddingHorizontal: 20, 
        marginVertical: 30
    },
    contenScrollView: {
        flexGrow : 1, 
        justifyContent : 'center'
    },
    topBarWrapper: {
        flexDirection: 'row', 
        flex: 1, 
        alignItems: 'center', 
        paddingHorizontal: 20
    },
    topBarTitle: {
        fontSize: 23, 
        color: '#2BBBAD', 
        fontWeight: 'bold'
    }
})
