import React, { useEffect, useState } from 'react'
import { Alert, StyleSheet, Text, TextInput, TouchableNativeFeedback, TouchableOpacity, useColorScheme, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import Axios from 'axios';

const RestApi = () => {
    useEffect(() => {
        getData();
    }, []);

    const [title, setTitle] = useState("");
    const [value, setValue] = useState("");
    const [books, setBooks] = useState([]);
    const [button, setButton] = useState("SIMPAN");
    const [selectedUser, setSelectedUser] = useState({});

    const submit = () => {
        const data = { title, value };
       
        if (button === 'SIMPAN') {
            Axios.post('https://achmadhilmy-sanbercode.my.id/api/v1/news', data)
            .then(res => {
                setTitle("");
                setValue("");
                getData();
            })
        } else if (button === 'UBAH'){
            Axios.put(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${selectedUser.id}`, data)
            .then(res => {
                setButton('SIMPAN');
                setTitle("");
                setValue("");
                getData();
            })
        } 
    }

    const getData = () => {
        Axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
        .then(res => {
            setBooks(res.data.data);
        })
    }

    const Item = ({id, title, value, onPress, onDelete}) => {
        return (
            <View>
                <View style={{flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
                    <View>
                        <TouchableOpacity onPress={onPress}>
                            <Text>ID : {id}</Text>
                            <Text>Title : {title}</Text>
                            <Text>Value : {value}</Text>
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity onPress={onDelete}>
                        <Text style={{fontSize: 30, fontWeight: 'bold'}}>Hapus</Text>
                    </TouchableOpacity>
                </View>
                <Text></Text>
            </View>
        );
    }

    const edit = (item) => {
        setSelectedUser(item);
        setTitle(item.title);
        setValue(item.value);
        setButton('UBAH');
    }

    const destroy = (item) => {
        Axios.delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${item.id}`)
        .then(res => {
            getData();
        })
    }

    return (
        <View>
            <ScrollView
                    contentContainerStyle={styles.contenScrollView}
                    showsVerticalScrollIndicator={false}
                    contentInsetAdjustmentBehavior="automatic">
                <View style={styles.wrapper}>
                    <TextInput style={styles.input} placeholderTextColor={'#A5A5A5'} placeholder={'Title'} value={title} onChangeText={(value) => setTitle(value)} />
                    <TextInput style={styles.input} placeholderTextColor={'#A5A5A5'} placeholder={'Description'} value={value} onChangeText={(value) => setValue(value)} />
                    <View style={{alignItems: 'center'}}>
                        <TouchableOpacity onPress={submit} style={{ width: 200, backgroundColor: '#2BBBAD', borderRadius: 25, paddingVertical: 13, marginTop: 10}}>
                            <Text style={{textAlign: 'center', color: 'white', fontWeight: 'bold', fontSize: 17}}>{button}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{borderBottomWidth: 1, marginTop: 30, marginBottom: 10}} />
                    {books.map(book => {
                        return (
                            <Item 
                                key={book.id} 
                                id={book.id}  
                                title={book.title} 
                                value={book.value} 
                                onPress={() => edit(book)} 
                                onDelete={() => Alert.alert(
                                    'Peringatan', 
                                    'Apakah anda yakin ???', 
                                    [
                                        {
                                            text: 'Tidak', 
                                            onPress: () => console.log('tidak')
                                        }, 
                                        {
                                            text: 'Ya', 
                                            onPress: () => destroy(book)
                                        }
                                    ]
                                )} 
                            />
                        )
                    })}
                </View>
            </ScrollView>
        </View>
    )
}

export default RestApi

const styles = StyleSheet.create({
    contenScrollView: {
        flexGrow : 1, 
        justifyContent : 'center'
    },
    wrapper: {
        flex: 1, 
        justifyContent: 'center', 
        paddingHorizontal: 20, 
        marginVertical: 30
    },
    input:{
        borderBottomWidth: 1, 
        borderBottomColor: '#A5A5A5', 
        fontSize: 14, 
        padding: 0, 
        marginTop: 33
    },
})
