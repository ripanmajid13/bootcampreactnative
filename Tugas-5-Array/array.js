// Soal No. 1 (Range) 
const range = (startNum = false, finishNum = false) => {
    if (startNum == false || finishNum == false) {
        return -1;
    } else {
        if (startNum <= finishNum) {
            let numMax = [];
            for (let angkaMax = startNum; angkaMax <= finishNum; angkaMax++) {
                numMax.push(angkaMax);
            }
            return numMax;
        } else {
            let numMin = [];
            for (let angkaMin = startNum; angkaMin >= finishNum; angkaMin--) {
                numMin.push(angkaMin);
            }
            return numMin;
        }
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal No. 2 (Range with Step)
const rangeWithStep = (startNum, finishNum, step) => {
    if (startNum >= finishNum) {
        let fs = [];
         for (let angkafs = startNum; angkafs >= finishNum; angkafs -= step) {
            fs.push(angkafs);   
        }
        return fs;
    } else {
        let sf = [];
        for (let angkasf = startNum; angkasf <= finishNum; angkasf += step) {
            sf.push(angkasf);   
        }
        return sf;
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal No. 3 (Sum of Range)
const sum = (startNum = false, finishNum = false, step = 1) => {
    if (startNum == false || finishNum == false) {
        if (startNum) {
            return startNum;
        } else if (finishNum) {
            return finishNum;
        } else {
            return 0;
        }
    } else {
        if (startNum <= finishNum) {
            let numMax = 0;
            for (let angkaMax = startNum; angkaMax <= finishNum; angkaMax += step) {
                numMax += angkaMax;
            }
            return numMax;
        } else {
            let numMin = 0;
            for (let angkaMin = startNum; angkaMin >= finishNum; angkaMin -= step) {
                numMin += angkaMin;
            }
            return numMin;
        }
    }
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array Multidimensi)
const dataHandling = (array) => {
    let data = '';
    array.forEach(element => {
        data += `Nomor ID: ${element[0]} \nNama Lengkap: ${element[1]} \nTTL: ${element[2]} ${element[3]} \nHobi: ${element[4]}\n\n`;
    });
    console.log(data);
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
dataHandling(input);

// Soal No. 5 (Balik Kata)
const balikKata = (str) => {
    let data = '';
    for (let i = str.length; i >= 0; i--) {
        if (typeof str[i] !== 'undefined') {
            data += str[i];
        }  
    }
    return data;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal No. 6 (Metode Array)
const dataHandling2 = (arr) => {
    arr.splice(1, 1, "Roman Alamsyah Elsharawy");
    arr.splice(2, 1, "Provinsi Bandar Lampung");
    arr.splice(4, 0, "Pria");
    arr.splice(5, 1, "SMA Internasional Metro");

    let bulan;
    switch (arr[3].split("/")[1]) {
        case "01":
            bulan = "Januari";
            break;
        case "02":
            bulan = "Februari";
            break;
        case "03":
            bulan = "Maret";
            break;
        case "04":
            bulan = "April";
            break;
        case "05":
            bulan = "Mei";
            break;
        case "06":
            bulan = "Juni";
            break;
        case "07":
            bulan = "Juli";
            break;
        case "08":
            bulan = "Agustus";
            break;
        case "09":
            bulan = "September";
            break;
        case "10":
            bulan = "Oktober";
            break;
        case "11":
            bulan = "November";
            break;
        case "12":
            bulan = "Desember";
            break;

        default:
            bulan = 'none';
            break;
    }

    console.log('/**');
    console.log(' * keluaran yang diharapkan (pada console)');
    console.log(' *');
    console.log(' *');
    console.log(' * '+JSON.stringify(arr));
    console.log(' * '+bulan);
    console.log(' * '+JSON.stringify(arr[3].split('/').sort(function (value1, value2) { return value2 - value1 } )));
    console.log(' * '+arr[3].split("/")[0]+'-'+arr[3].split("/")[1]+'-'+arr[3].split("/")[2]);
    console.log(' * '+arr[1].split(" ")[0]+' '+arr[1].split(" ")[0]);
    console.log(' */');
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);