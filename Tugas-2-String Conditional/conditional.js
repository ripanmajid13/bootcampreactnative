// Jawaban soal If-else
var nama    = "John";
var peran   = "";

if (nama.length) {
    if (peran.length) {
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
        if (peran == 'Penyihir') {
            console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
        } else if (peran == 'Guard') {
            console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`);
        } else if (peran == 'Werewolf') {
            console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`);
        } else {
            console.log(`Halo ${peran} ${nama}, peranmu tidak teridentifikasi.`);
        }
    } else {
        console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
    }
} else {
    console.log('Nama harus diisi!');
}


// Jawaban soal Switch Case
var hari    = 21; 
var bulan   = 1; 
var tahun   = 1945;

switch(true) { 
    case (hari >= 1 && hari <= 31):  { hari; break; }
    default:  { hari = 'unknown'; }
}

switch(bulan) {
    case 1:   { bulan = 'Januari'; break; }
    case 2:   { bulan = 'Februari'; break; }
    case 3:   { bulan = 'Maret'; break; }
    case 4:   { bulan = 'April'; break; }
    case 5:   { bulan = 'Mei'; break; }
    case 6:   { bulan = 'Juni'; break; }
    case 7:   { bulan = 'Juli'; break; }
    case 8:   { bulan = 'Agustus'; break; }
    case 9:   { bulan = 'September'; break; }
    case 10:  { bulan = 'Oktober'; break; }
    case 11:  { bulan = 'November'; break; }
    case 12:  { bulan = 'Desember'; break; }
    default:  { bulan = 'unknown'; }
}

switch(true) { 
    case (tahun >= 1900 && tahun <= 2200):  { tahun; break; }
    default:  { tahun = 'unknown'; }
}

var hasil = hari + ' ' + bulan + ' ' + tahun;
console.log(hasil);
console.log('');