// No. 1 Looping While 
console.log('');
console.log('No. 1 Looping While');
console.log('LOOPING PERTAMA');
var lp1 = 1;
while(lp1 <= 20 ) { 
    if (lp1 % 2 == 0) {
        console.log(lp1+' - I love coding');
    }
    lp1++; 
}
console.log('LOOPING KEDUA');
var lp2 = 20;
while(lp2 >= 1) { 
    if (lp2 % 2 == 0) {
        console.log(lp2+' - I will become a mobile developer');
    }
    lp2--; 
}
console.log('');

// No. 2 Looping menggunakan for
console.log('No. 2 Looping menggunakan for');
for(var angka = 1; angka <= 20; angka++) {
    if (angka % 2 != 0) {
        if (angka % 3 == 0) {
            console.log(angka+' - I Love Coding ');
        } else {
            console.log(angka+' - Santai');
        }
    } else {
        console.log(angka+' - Berkualitas');
    }
} 
console.log('');

// No. 3 Membuat Persegi Panjang #
console.log('No. 3 Membuat Persegi Panjang #');
var h3 = '';
for(var a3 = 0; a3 < 4; a3++) {
    for (var b3 = 0; b3 < 7; b3++) {
        h3 += '#';
    }
    h3 += '\n';
} 
console.log(h3);
console.log('');

// No. 4 Membuat Tangga 
console.log('No. 4 Membuat Tangga');
var h4 = '';
for (var a4 = 0; a4 < 7; a4++) {
    for (var b4 = 0; b4 <= a4; b4++) {
        h4 += '#';
    }
    h4 += '\n';
}
console.log(h4);
console.log('');

// No. 5 Membuat Papan Catur
console.log('No. 5 Membuat Papan Catur');
var h5 = '';
for(var a5 = 1; a5 < 9; a5++) {
    for (var b5 = 0; b5 < 4; b5++) {
        if (a5 % 2 == 0) {
            h5 += '# ';
        } else {
            h5 += ' #';
        }
    }
    h5 += '\n';
} 
console.log(h5);
console.log('');