const readBookPromise = (time, book) => {
    console.log(`Saya membaca ${book.name}`);
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let sisaWaktu = time - book.timeSpent;
            if (sisaWaktu >= 0) {
                sisaWaktu = time - book.timeSpent;
                console.log(`Saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`);
                resolve(sisaWaktu);
            } else {
                console.log('Waktu saya habis!');
                reject(sisaWaktu);
            }
        }, book.timeSpent);
    })
}

module.exports = readBookPromise;