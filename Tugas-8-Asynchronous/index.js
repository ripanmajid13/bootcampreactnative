let readBooks = require('./callback.js');

let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];


let t = 10000, i = 0; 
let baca = () => { 
    readBooks(t, books[i], (sisa) => { 
        t = sisa, i++ 
        if(i < books.length) { 
            baca(); 
        } 
    }) 
} 
baca();

