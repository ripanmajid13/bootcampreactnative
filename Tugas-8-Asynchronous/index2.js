let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let t = 10000, i = 0; 
let cari = () => { 
    readBooksPromise(t, books[i]) 
    .then((sisa) => { 
        if(i < books.length) { 
            t = sisa, i ++ 
            cari() 
        } 
    })
    .catch((r) => console.log(r)) 
} 

cari()